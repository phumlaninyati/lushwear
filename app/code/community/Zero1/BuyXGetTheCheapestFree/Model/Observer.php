<?php
/**
 * Zero1_BuyXGetTheCheapestFree_Model_Observer
 *
 * @category    Zero1
 * @package     Zero1_BuyXGetTheCheapestFree
 * @copyright   Copyright (c) 2012 Zero1 Ltd. (http://www.zero1.co.uk)
 */
class Zero1_BuyXGetTheCheapestFree_Model_Observer extends Mage_SalesRule_Model_Validator
{
    /**
     * Bespoke rule definition
     */
	const BUY_X_GET_CHEAPEST_FREE = 'buy_x_get_cheapest_free';
	
	/**
	 * Storage for the current ruleId (Used in _filter)
	 *
	 * @var int
	 */
	var $_ruleId;
	
	/**
	 * Handles the "adminhtml_block_salesrule_actions_prepareform" event
	 *
	 * @param Varien_Event_Observer $observer
	 * @return null
	 */
	public function adminhtml_block_salesrule_actions_prepareform(Varien_Event_Observer $observer)
	{
		$field = $observer->getForm()->getElement('simple_action');
		
		$options = $field->getValues();		
		$options[] = array(
				'value' => self::BUY_X_GET_CHEAPEST_FREE,
				'label' => 'Zero-1 - Buy X get the cheapest free'
		);
		
		$field->setValues($options);
	}
	
	/**
	 * Sort the input items by price
	 *
	 * @param Mage_Sales_Model_Quote_Item $a
	 * @param Mage_Sales_Model_Quote_Item $b
	 * @return int
	 */
	private function _sort(Mage_Sales_Model_Quote_Item $a, Mage_Sales_Model_Quote_Item $b)
	{		
		if($a->getPrice() == $b->getPrice())
			return 0;
		
		return ($a->getPrice() < $b->getPrice()) ? -1 : 1;
	}
	
	/**
	 * Filter the items to only ones related to the rule
	 *
	 * @param Mage_Sales_Model_Quote_Item $a
	 * @return bool
	 */
	private function _filter(Mage_Sales_Model_Quote_Item $a)
	{		
		if(in_array($this->_ruleId, explode(',', $a->getAppliedRuleIds())))
			return true;
		
		return false;
	}
	
	/**
	 * Handles the "salesrule_validator_process" event
	 *
	 * @param Varien_Event_Observer $observer
	 * @return null
	 */
	public function salesrule_validator_process(Varien_Event_Observer $observer)
	{
		// Get the event arguments
		$rule		= $observer->getRule();
		/* @var $item Mage_Sales_Model_Quote_Item */
		$item		= $observer->getItem();
		$address	= $observer->getAddress();
		$quote		= $observer->getQuote();
        $qty		= $observer->getQty();
        $result		= $observer->getResult();
        
        // Get all the items in the current quote
		$quote_items = $quote->getAllItems();
        
		// Store the rule ID for usage in callbacks
		$this->_ruleId = $rule->getId();

		switch($rule->getSimpleAction())
		{
			case self::BUY_X_GET_CHEAPEST_FREE :

				if($rule->getDiscountStep() <= 0)
					return;	// Invalid rule
				
				// Remove items that are not used by this rule
				$rule_items = array_filter($quote_items, array($this, '_filter'));
				
				// Sort by price, lowest to highest
				usort($rule_items, array($this, '_sort'));
				
				// Get the number of products to discount
				$products_to_discount = 0;
				foreach($rule_items as $rule_item)
				{
					// Do not include the simples of a configurable
					if($rule_item->getParentItem())
						continue;
				
					$products_to_discount += $rule_item->getQty();
				}
				
				$products_to_discount = floor($products_to_discount / $rule->getDiscountStep());
				
				if($rule->getDiscountQty() > 0)
					$products_to_discount = min($products_to_discount, $rule->getDiscountQty());
				
				// Identify the products that should be discounted
				// array( $id => $qty )
				$items_discounted = array();
				$discounts_applied = 0;
				
				foreach($rule_items as $rule_item)
				{
					// Do not include the simples of a configurable
					if($rule_item->getParentItem())
						continue;
					
					if($discounts_applied >= $products_to_discount)
						break; // All the discounts have been applied, stop processing more
					
					// Get the possible amount of discounts that can be applied to this item
					// Taking in to account the quanity assigned to the cart
					$amount = min($rule_item->getQty(), ($products_to_discount - $discounts_applied));
					
					if($amount > 0)
					{
						$items_discounted[$rule_item->getId()] = $amount;
						$discounts_applied += $amount;
					}
				}

				if(array_key_exists($item->getId(), $items_discounted))
				{
					//gets the item price value that we should be applying discount to
					//this is effected by System > Config > Tax > Calculation Settings > Apply Discount On Prices
					$itemPrice = $this->_getItemPrice($item);
					$baseItemPrice = $this->_getItemBasePrice($item);

					//calculate discount amount, making sure >=0
					$ruleDiscountPercent = max(0, $rule->getDiscountAmount());
					$ruleDiscountDecimal = $ruleDiscountPercent/100;
					// amount = ( (item price - existing discounts) X % ) X Number of items to apply to
					$discountAmount = (($itemPrice - $item->getDiscountAmount()) * $ruleDiscountDecimal) * $items_discounted[$item->getId()];
					$baseDiscountAmount = ($baseItemPrice - $item->getBaseDiscountAmount()) * $ruleDiscountDecimal;

					/**
					 *These Values are modified in core, but aren't accessible from observer
					 *
					$originalDiscountAmount = 0;
					$baseOriginalDiscountAmount = 0;
					$itemOriginalPrice = $this->_getItemOriginalPrice($item);
					$baseItemOriginalPrice  = $this->_getItemBaseOriginalPrice($item);
					$originalDiscountAmount = ($itemOriginalPrice - $item->getBaseDiscountAmount()) * $ruleDiscountDecimal;
					$baseOriginalDiscountAmount = ($baseItemOriginalPrice - $item->getDiscountAmount()) * $ruleDiscountDecimal;
					 */

//					Mage::log(PHP_EOL.
//						'Item name: '.$item->getName().PHP_EOL.
//						'Item Qty: '.$item->getQty().PHP_EOL.
//						'Qty to be discounted: '.$items_discounted[$item->getId()].PHP_EOL.
//						'$itemPrice: '.$itemPrice.PHP_EOL.
//						'$baseItemPrice: '.$baseItemPrice.PHP_EOL.
//						'$ruleDiscountPercent: '.$ruleDiscountPercent.PHP_EOL.
//						'$ruleDiscountDecimal: '.$ruleDiscountDecimal.PHP_EOL.
//						'$discountAmount: '.$discountAmount.PHP_EOL.
//						'========================================================'.PHP_EOL.
//						'Discount Amount: '.$discountAmount.PHP_EOL.
//						'Base Discount Amount: '.$baseDiscountAmount.PHP_EOL,
//						Zend_Log::DEBUG, 'getCheepestFree.log', true);

					//update item object
					$itemDiscountPercent = min(100, $item->getDiscountPercent()+$ruleDiscountPercent);
					$item->setDiscountPercent($itemDiscountPercent);

					//update the result object
					$result->setDiscountAmount($discountAmount);
					$result->setBaseDiscountAmount($baseDiscountAmount);
				}
				break;
		}
	}

	/**
	 * @param Mage_Sales_Model_Quote_Item_Abstract $item
	 * @return float
	 */
	protected function _getItemPrice($item)
	{
		$price = $item->getDiscountCalculationPrice();
		$calcPrice = $item->getCalculationPrice();
		return ($price !== null) ? $price : $calcPrice;
	}

	/**
	 * Return item base price
	 *
	 * @param Mage_Sales_Model_Quote_Item_Abstract $item
	 * @return float
	 */
	protected function _getItemBasePrice($item)
	{
		$price = $item->getDiscountCalculationPrice();
		return ($price !== null) ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
	}

	/**
	 * Return item original price
	 *
	 * @param Mage_Sales_Model_Quote_Item_Abstract $item
	 * @return float
	 */
	protected function _getItemOriginalPrice($item)
	{
		return Mage::helper('tax')->getPrice($item, $item->getOriginalPrice(), true);
	}

	/**
	 * Return item base original price
	 *
	 * @param Mage_Sales_Model_Quote_Item_Abstract $item
	 * @return float
	 */
	protected function _getItemBaseOriginalPrice($item)
	{
		return Mage::helper('tax')->getPrice($item, $item->getBaseOriginalPrice(), true);
	}
}