<?php

$installer = $this;

$installer->startSetup();
$setup = Mage::getModel('customer/entity_setup', 'core_setup');
$setup->addAttribute('customer', 'dresssize', array(
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Dress Size',
	'global' => 1,
	'visible' => 1,
	'required' => 0,
	'user_defined' => 1,
	'default' => '',
	'visible_on_front' => 1,
    'source' =>	 'profile/entity_dresssize',
));
$setup->addAttribute('customer', 'brasize', array(
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Bra Size',
	'global' => 1,
	'visible' => 1,
	'required' => 0,
	'user_defined' => 1,
	'default' => '',
	'visible_on_front' => 1,
    'source' =>	 'profile/entity_brasize',
));

if (version_compare(Mage::getVersion(), '1.6.0', '<='))
{
	$customer = Mage::getModel('customer/customer');
	$attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
	$setup->addAttributeToSet('customer', $attrSetId, 'General', 'dresssize');
	$setup->addAttributeToSet('customer', $attrSetId, 'General', 'brasize');
}

if (version_compare(Mage::getVersion(), '1.4.2', '>='))
{
	Mage::getSingleton('eav/config')
	->getAttribute('customer', 'dresssize')
	->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'))
	->save();
	Mage::getSingleton('eav/config')
	->getAttribute('customer', 'brasize')
	->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'))
	->save();

}

$tablequote = $this->getTable('sales/quote');
$installer->run("
ALTER TABLE  $tablequote ADD  `customer_dresssize` VARCHAR( 64 ) NOT NULL
");
$installer->run("
ALTER TABLE  $tablequote ADD  `customer_brasize` VARCHAR( 64 ) NOT NULL
");

$installer->endSetup();
?>