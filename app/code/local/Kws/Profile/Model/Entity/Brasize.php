<?php
class Kws_Profile_Model_Entity_Brasize extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getDefaultEntities(){
 
        return array(
 
                'brasize'=>array(
                        'type'=> 'static',
                        'label'=> 'Bra Size',
                        'visiable' => true,
                        'required' => true,
                        'sort_order' => 80,
                )
        );
    }
}