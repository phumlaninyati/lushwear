<?php
class Kws_Profile_Model_Entity_Dresssize extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getDefaultEntities(){
 
        return array(
 
                'dresssize'=>array(
                        'type'=> 'static',
                        'label'=> 'Dress Size',
                        'visiable' => true,
                        'required' => true,
                        'sort_order' => 80,
                )
        );
    }
}